#badwords－违禁词，敏感词库
论坛、社交网站、博客、微博等允许用户生成内容的网站，常常遇到屏蔽违禁词、敏感词、广告、色情等文字的问题。这里给出一份违禁词列表，供大家参考。

共分以下几类：
* 政治
* 色情
* 违法犯罪
* 广告
* 脏话

#如何使用
发布违禁词本身就是违禁的。。。所以生成的词库都用base64编码。
大家使用时，可以用 decrypt.php 解码，生成的rawbadwords_out.txt文件里即是违禁词。
或者使用 [在线服务](http://tool.chinaz.com/tools/base64.aspx) 解码，将pubbadwords.txt的内容粘贴在右边框里，点击“Base64解密”，违禁词就出现在左边框里。

#ChangeLog
2016-04-16 发布第一份违禁词库，共1673条。